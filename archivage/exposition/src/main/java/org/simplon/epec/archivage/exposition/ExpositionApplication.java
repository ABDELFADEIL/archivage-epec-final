package org.simplon.epec.archivage.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;



@SpringBootApplication
@EntityScan(basePackages = {"org.simplon.epec.archivage.*"})
@ComponentScan(basePackages = {"org.simplon.epec.archivage.*"})
@EnableJpaRepositories(basePackages = {"org.simplon.epec.archivage.*"})
public class ExpositionApplication  extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
  //  @Autowired
   // private  ClassificationJpaNatureRepository classificationJpaNatureRepository;


    public static void main(String[] args) {
        SpringApplication.run(ExpositionApplication.class, args);
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }


    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ExpositionApplication.class);
    }

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    public void run(String... args) throws Exception {
        //System.out.println("//////////////////////////////////////////////////////////////////////////////////////////////////////////////////");

       // ClassificationNature c = classificationJpaNatureRepository.findById(4821085995791567000L).get();
       // System.out.println(c);

    }




}
