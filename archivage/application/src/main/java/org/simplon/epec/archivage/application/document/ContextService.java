package org.simplon.epec.archivage.application.document;

import org.simplon.epec.archivage.domain.document.entity.Context;

public interface ContextService {

    Context createContext(Context context);
    Context saveContext(Context context);

}
